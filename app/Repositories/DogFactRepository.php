<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Models\DogFact;
use Illuminate\Pagination\LengthAwarePaginator;

class DogFactRepository
{
    public function getAllWithPagination(array $options = [], array $relations = []): ?LengthAwarePaginator
    {
        $pageSize = $options['pageSize'] ?? config('pagination.default_size');
        $page = $options['page'] ?? 1;

        return DogFact::with($relations)->paginate($pageSize, ['*'], 'page', $page);
    }

    public function getById(int $id, array $relations = []): ?DogFact
    {
        return DogFact::with($relations)->find($id);
    }

    public function store(array $options): ?DogFact
    {
        $DogFact = new DogFact();

        return $this->setFieldsAndPersist($DogFact, $options);
    }

    public function update(DogFact $DogFact, array $options): ?DogFact
    {
        return $this->setFieldsAndPersist($DogFact, $options);
    }

    protected function setFieldsAndPersist(DogFact $DogFact, array $options): ?DogFact
    {
        $DogFact->fact = $options['fact'];

        if ($DogFact->save()) {
            return $DogFact;
        }

        return null;
    }

    public function destroy(DogFact $DogFact): bool
    {
        return $DogFact->delete();
    }
}
