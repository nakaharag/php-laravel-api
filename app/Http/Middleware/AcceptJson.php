<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Testing\Fluent\AssertableJson;

class AcceptJson
{
    public function handle($request, Closure $next)
    {
       $methods = [
           'POST', 'PUT', 'PATCH'
       ];

        if (in_array($request->method(), $methods)) {
            if(!$request->isJson()) {
                return response()->json(['Invalid json request'], 422);
            }
        }

        return $next($request);
    }
}
