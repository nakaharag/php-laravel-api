<?php

namespace App\Http\Controllers\API\V1\DogFacts\Rest;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\DogFacts\Rest\StoreRequest;
use App\Repositories\DogFactRepository;
use Exception;

use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class StoreController extends Controller
{
    protected DogFactRepository $repository;

    public function __construct(DogFactRepository $dogFactRepository)
    {
        $this->repository = $dogFactRepository;
    }

    public function __invoke(StoreRequest $request): Response
    {
        $validator = Validator::make($request->all(), $request->rules());

        if ($validator->fails()) {
            return response()->json([
                "error" => 'validation_error',
                "message" => $validator->errors(),
            ], 422);
        }

        try {
            $dogFact = $this->repository->store($request->all());
        } catch (Exception $exception) {
            $response = $exception->getMessage();
            return response()->json($response, 500);
        }

        if (!$dogFact) {
            $response = 'Dog fact could not be saved';
            return response()->json($response, 500);
        }

        $response = $dogFact;
        return response()->json($dogFact, 200);
    }
}
