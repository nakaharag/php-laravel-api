<?php

namespace App\Http\Controllers\API\V1\DogFacts\Rest;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\DogFacts\Rest\UpdateRequest;
use App\Repositories\DogFactRepository;
use Exception;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class UpdateController extends Controller
{
    protected DogFactRepository $repository;

    public function __construct(DogFactRepository $dogFactRepository)
    {
        $this->repository = $dogFactRepository;
    }

    public function __invoke(int $id, UpdateRequest $request): Response
    {
        $validator = Validator::make($request->all(), $request->rules());

        if ($validator->fails()) {
            return response()->json([
                "error" => 'validation_error',
                "message" => $validator->errors(),
            ], 422);
        }

        $dogFact = $this->repository->getById($id);

        if (!$dogFact) {
            $response = "Dog fact not found";
            return response()->json($response, 404);
        }

        try {
            $updated = $this->repository->update($dogFact, $request->all());
        } catch (Exception $exception) {
            $response = $exception->getMessage();
            return response()->json($response, 500);
        }

        if (!$updated) {
            $response = "Dog fact could not be updated";
            return response()->json($response, 500);
        }

        return response()->json($updated);
    }
}
